moment = require 'moment'
module.exports = (schema, options) ->
  updatedAt = 'updatedAt'
  createdAt = 'createdAt'
  updatedAtType = Number
  createdAtType = Number
  if typeof options == 'object'
    if typeof options.updatedAt == 'string'
      updatedAt = options.updatedAt
    else if typeof options.updatedAt == 'object'
      updatedAt = options.updatedAt.name or updatedAt
      updatedAtType = options.updatedAt.type or updatedAtType
    if typeof options.createdAt == 'string'
      createdAt = options.createdAt
    else if typeof options.createdAt == 'object'
      createdAt = options.createdAt.name or createdAt
      createdAtType = options.createdAt.type or createdAtType
  dataObj = {}
  dataObj[updatedAt] = updatedAtType
  if schema.path(createdAt)
    schema.add dataObj
    schema.pre 'save', (next) ->
      if @isNew
        @[updatedAt] = @[createdAt]
      else if @isModified()
        @[updatedAt] = moment().format('x')
      next()
  else
    dataObj[createdAt] = createdAtType
    schema.add dataObj
    schema.pre 'save', (next) ->
      if !@[createdAt]
        @[createdAt] = @[updatedAt] = moment().format('x')
      else if @isModified()
        @[updatedAt] = moment().format('x')
      next()
  schema.pre 'findOneAndUpdate', (next) ->
    if @op == 'findOneAndUpdate'
      @_update = @_update or {}
      @_update[updatedAt] = moment().format('x')
      @_update['$setOnInsert'] = @_update['$setOnInsert'] or {}
      @_update['$setOnInsert'][createdAt] = moment().format('x')
    next()
    return
  schema.pre 'update', (next) ->
    if @op == 'update'
      @_update = @_update or {}
      @_update[updatedAt] = moment().format('x')
      @_update['$setOnInsert'] = @_update['$setOnInsert'] or {}
      @_update['$setOnInsert'][createdAt] = moment().format('x')
    next()
  if !schema.methods.hasOwnProperty('touch')

    schema.methods.touch = (callback) ->
      @[updatedAt] = moment().format('x')
      @save callback